package com.silasett.login_demo.controller;

import com.silasett.login_demo.domain.User;
import com.silasett.login_demo.service.UserService;
import com.silasett.login_demo.service.util.JsonObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.math.BigInteger;
import java.util.List;

import static java.util.Objects.isNull;

@RestController
@RequestMapping("/api/users")
public class UserController {
  @Autowired
  UserService userService;

  //  @RequestMapping(value = "/all", method = RequestMethod.GET)
  @RequestMapping("/all")
  public ResponseEntity<List<User>> showAllUser() {
    List<User> users = userService.showAllUsers();
    return new ResponseEntity<>(users, HttpStatus.OK);
  }

  @RequestMapping(value = "/username/{id}", method = RequestMethod.GET)
  public ResponseEntity<User> showUser(@PathVariable("id") String username) {
    User user = userService.showUser(username);
    return new ResponseEntity<>(user, HttpStatus.OK);
  }

  @PostMapping("/register")
  public ResponseEntity<String> register(@RequestBody String jsonStringUser) {
    return userService.isRegisterSuccess(jsonStringUser) ?
      new ResponseEntity<>("Register Successfully", HttpStatus.OK) :
      new ResponseEntity<>(
        "Register Failed.\n" +
          "\t- Password and Confirm Password might not match.\n" +
          "\t- Email might exist in the system.\n" +
          "\t- Username might exist in the system.",
        HttpStatus.UNPROCESSABLE_ENTITY
      );
  }

  @PostMapping("/login")
  public ResponseEntity<String> login(@RequestBody String userJsonString) {
    User user = userService.login(userJsonString);
    System.out.println("login: " + user);

    if (!isNull(user)) {
      return new ResponseEntity<>("Login successfully", HttpStatus.OK);
    } else {
      return new ResponseEntity<>("Login failed", HttpStatus.NO_CONTENT);
    }
  }

  @PutMapping("/password")
  public ResponseEntity<String> changePassword(@RequestBody String usernameAndOldPasswordAndNewPasswordAndConfirmPassword) {
    String passwordChangedStatus = userService.changePassword(usernameAndOldPasswordAndNewPasswordAndConfirmPassword);

    // Status = 1 is success, Status = 2 is failed
    if (passwordChangedStatus.equals("Success")) {
      return new ResponseEntity<>("Changed password successfully", HttpStatus.OK);
    } else if (passwordChangedStatus.equals("Fail")) {
      return new ResponseEntity<>("Changed password failed", HttpStatus.UNPROCESSABLE_ENTITY);
    } else {
      return new ResponseEntity<>("New Password and Confirm Password not matched", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  @DeleteMapping("/destroy")
  public ResponseEntity<String> deleteUser(@RequestParam String username) {
    User user = userService.deleteUser(username);
    if (!isNull(user)) {
      return new ResponseEntity<>("Delete " + username + " successfully.", HttpStatus.OK);
    } else {
      return new ResponseEntity<>("username " + username + " is not exist.", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }
}
