package com.silasett.login_demo.repository;

import com.silasett.login_demo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
  // Show all users
  List<User> findAll();

  // Show user by username
  User findByUsername(String username);

  // Login case with normal method
  Optional<User> findByUsernameAndPassword(String username, String password);

  // Login case with native query
  @Query(nativeQuery = true,
    value = "SELECT (count(*) = 1) " +
      "FROM users u " +
      "WHERE u.username = :uname AND u.password = :passwd"
  )
  BigInteger isIdentifiedUser(@Param("uname") String username, @Param("passwd") String password);

}
