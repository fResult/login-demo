package com.silasett.login_demo.service.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.silasett.login_demo.domain.User;
import com.silasett.login_demo.domain.UserRegistryRequest;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class JsonObjectMapper {
  public User parseJsonToUser(String jsonString) {
    ObjectMapper mapper = new ObjectMapper();
    User user = null;

    try {
      user = mapper.readValue(jsonString, User.class);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return user;
  }

  public UserRegistryRequest parseJsonToUserRegistryRequest(String jsonString) {
    ObjectMapper mapper = new ObjectMapper();
    UserRegistryRequest userRegistryRequest = null;

    try {
      userRegistryRequest = mapper.readValue(jsonString, UserRegistryRequest.class);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return userRegistryRequest;
  }
}
