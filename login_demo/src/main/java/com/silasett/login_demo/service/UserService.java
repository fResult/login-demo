package com.silasett.login_demo.service;

import com.silasett.login_demo.domain.User;
import com.silasett.login_demo.domain.UserRegistryRequest;
import com.silasett.login_demo.repository.UserRepository;
import com.silasett.login_demo.service.security.PasswordUtility;
import com.silasett.login_demo.service.util.JsonObjectMapper;
import com.silasett.login_demo.service.util.StringManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;

@Service
public class UserService {

  @Autowired
  UserRepository userRepository;

  @Autowired
  JsonObjectMapper jsonObjectMapper;

  @Autowired
  PasswordUtility passwordUtility;

  @Autowired
  StringManager stringManager;

  public List<User> showAllUsers() {
    List<User> users = userRepository.findAll();

    return users;
  }

  public User showUser(String username) {
    User user = userRepository.findByUsername(username);
    return user;
  }

  public boolean isRegisterSuccess(String jsonRegistryString) {
    User user = null;
    UserRegistryRequest userRegistryRequest = jsonObjectMapper.parseJsonToUserRegistryRequest(jsonRegistryString);

    String passwordEncrypted = passwordUtility.encryptPassword(userRegistryRequest.getPassword());
    boolean isPasswordMatched = passwordUtility.matchPasswordWithConfirmPasswordIsTrue(userRegistryRequest.getPassword(),
      userRegistryRequest.getConfirmPassword());

    if (isPasswordMatched) {

      user = new User(userRegistryRequest.getUsername(), passwordEncrypted, userRegistryRequest.getFirstName(),
        userRegistryRequest.getLastName(), userRegistryRequest.getEmail());

      userRepository.save(user);
      return true;
    } else {
      return false;
    }
  }

  public User login(String userJsonString) {
    User user = jsonObjectMapper.parseJsonToUser(userJsonString);
    User userFromDb = userRepository.findByUsername(user.getUsername());
    String passwordEncrypted = passwordUtility.encryptPassword(user.getPassword());

    if (passwordUtility.matches(user.getPassword(), userFromDb.getPassword())) {
      User userLogin = userRepository.findByUsername(user.getUsername());
      return userLogin;
    } else {
      return null;
    }
  }

  public String changePassword(String usernameAndOldPasswordAndNewPasswordAndConfirmPassword) {

    String[] parts = stringManager.splitStringOfChangePasswordCase(usernameAndOldPasswordAndNewPasswordAndConfirmPassword);
    String username = parts[0];
    String oldPassword = parts[1];
    String newPassword = parts[2];
    String confirmPassword = parts[3];

    User user = userRepository.findByUsername(username);
    boolean passwordMatched = passwordUtility.matchPasswordWithConfirmPasswordIsTrue(newPassword, confirmPassword);

    if (passwordMatched) {
      String passwordDb = user.getPassword();

      if (passwordUtility.matches(oldPassword, passwordDb)) {
        String passwordEncrypted = passwordUtility.encryptPassword(newPassword);
        user.setPassword(passwordEncrypted);
        userRepository.save(user);
        return "Success";
      } else {
        return "Fail";
      }
    } else {
      return "Password not matched";
    }
  }

  public User deleteUser(String username) {
    User user = userRepository.findByUsername(username);

    if (!isNull(user)) {
      userRepository.delete(user);
      return user;
    } else {
      return null;
    }
  }
}
