package com.silasett.login_demo.service.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;


@Service
public class PasswordUtility extends BCryptPasswordEncoder {

  public BCryptPasswordEncoder getBCryptEncoder() {

    BCryptPasswordEncoder encoder = null;

    if (isNull(encoder)) {
      encoder = new BCryptPasswordEncoder();
      return encoder;
    } else {
      return encoder;
    }
  }

  public String encryptPassword(String password) {
    return this.getBCryptEncoder().encode(password);
  }

  public boolean matchPasswordWithConfirmPasswordIsTrue(String password, String confirmPassword) {
    return password.equals(confirmPassword) ? true : false;
  }
}
