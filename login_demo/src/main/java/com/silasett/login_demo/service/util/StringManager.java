package com.silasett.login_demo.service.util;

import org.springframework.stereotype.Service;

@Service
public class StringManager {
  public String[] splitStringOfChangePasswordCase(String usernameAndOldPasswordAndNewPasswordAndConfirmPassword) {
    return usernameAndOldPasswordAndNewPasswordAndConfirmPassword.split("::");
  }
}
