  CREATE SCHEMA login_demo;
USE login_demo

CREATE TABLE users (
  username    VARCHAR(20),
  password    VARCHAR(75)   NOT NULL,
  email       VARCHAR(50)   NOT NULL,
  first_name  VARCHAR(25)   NOT NULL,
  last_name   VARCHAR(30)   NOT NULL,
  PRIMARY KEY (username)
);

INSERT INTO users
VALUES ('Korn704', 'fResult', 'Styxmaz@gmail.com', 'Sila', 'Setthakan-anan'),
      ('KornZilla', 'fResult', 'Styxmaz@gmail.com', 'Sila', 'Setthakan-anan');
